To run make sure you have python3 and pipenv installed:
```
    python -m pip install pipenv
```

Then you can run it by:
```
    make run
```

Kubuntu needs this first:
```
    sudo apt-get install portaudio19-dev
```
