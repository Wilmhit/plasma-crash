# https://python-sounddevice.readthedocs.io/
import sounddevice as sd

RATE = 44100

SEC = 0.1
# This delay seems to be highiest causing crash. Feel free to change it.


sd.default.samplerate = RATE
sd.default.channels = 1

while True:
    audio = sd.rec(int(RATE*SEC), blocking=True)
    sd.play(audio, blocking=True)
